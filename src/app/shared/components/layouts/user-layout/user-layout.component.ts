import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../../service/users.service';
import { QuestionsService } from '../../../../service/questions.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-user-layout',
  templateUrl: './user-layout.component.html',
  styleUrls: ['./user-layout.component.scss'],
})
export class UserLayoutComponent implements OnInit {
  users: any;
  quizs: any;
  questions: any;
  questionsSubmit: any;
  score: number = 0;

  login = {
    is: false,
    link: 'login',
    icon: 'login',
    name: 'Đăng nhập',
  };

  constructor(
    private user: UsersService,
    private question: QuestionsService,
    private authService: AuthService
  ) {
    this.user.getData().subscribe((data) => {
      this.users = JSON.stringify(data);
    });

    this.question.getData().subscribe((data) => {
      this.questions = this.getRandom(data, 5);
    });
  }

  addDiem(form: any) {
    this.questionsSubmit = form.value;
    for (const key in this.questionsSubmit) {
      if (Object.prototype.hasOwnProperty.call(this.questionsSubmit, key)) {
        const element = this.questionsSubmit[key];
        for (const question of this.questions) {
          if (question.id == key) {
            if (question.correctIndex == element) this.score++;
          }
        }
      }
    }
    console.log(this.score);
  }

  getRandom(arr: any, n: any) {
    var result = new Array(n),
      len = arr.length,
      taken = new Array(len);
    if (n > len)
      throw new RangeError('getRandom: more elements taken than available');
    while (n--) {
      var x = Math.floor(Math.random() * len);
      result[n] = arr[x in taken ? taken[x] : x];
      taken[x] = --len in taken ? taken[len] : len;
    }
    return result;
  }

  ngOnInit() {
    this.authService.isAuthenticated().then((authenticated: boolean) => {
      if (authenticated) {
        this.login.is = true;
        this.login.link = 'logout';
        this.login.icon = 'logout';
        this.login.name = 'Đăng xuất';
      }
    });
  }
}
