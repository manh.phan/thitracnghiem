import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { AuthService } from 'src/app/service/auth.service';
import { UsersService } from 'src/app/service/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  hide: boolean = true;
  dataLogin = this.formBuilder.group({
    username: ['', [Validators.required]],
    password: ['', [Validators.required]],
  });
  horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  durationInSeconds = 1;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    private usersService: UsersService,
    private location: Location,
    private _snackBar: MatSnackBar,
  ) {}

  ngOnInit(): void {
    this.usersService.getData().subscribe((data) => console.log(data));
  }

  login() {
    this.usersService.getData().subscribe(
      (data) => {
        data.forEach((element) => {
          if (
            element.username == this.dataLogin.get('username')?.value &&
            element.password == this.dataLogin.get('password')?.value
          ) {
            this.authService.Login(element.id);
            this.location.back();
            return;
          }
        });
        this.authService.isAuthenticated().then((authenticated: boolean) => {
          if (!authenticated) this.openSnackBar();
        });
        
      },
      (error) => console.error(error)
    );
  }

  clear() {
    this.location.back();
  }

  openSnackBar() {
    this._snackBar.open('Sai tài khoản!!', 'OK', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration: this.durationInSeconds * 1000,
    });
  }
}
