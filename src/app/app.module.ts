import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from './shared/shared.module';
import { ViewsModule } from './views/views.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ScoreModule } from './views/score/score.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    ViewsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ScoreModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
