export interface Questions {
  id: string;
  question: string;
  answers1: string;
  answers2: string;
  answers3: string;
  answers4: string;
  correctIndex: string;
  subject: Subject;
}

export const Questions = [
  'id',
  'question',
  'answers1',
  'answers2',
  'answers3',
  'answers4',
  'correctIndex',
  'subject'
];

export interface Subject {
  id: string;
  name: string;
}