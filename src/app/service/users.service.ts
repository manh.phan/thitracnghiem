import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from "./user";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private http: HttpClient) {}

  private url = 'http://localhost:8081/api';
  getData() {
    return this.http.get<User[]>(this.url + '/users');
  }
  getuser(id: string) {
    return this.http.get<User>(`${this.url}/user/${id}`);
  }

  addData(data: any | null): Observable<User> {
    return this.http.post<User>(this.url + '/user/save', data);
  }

  updateUser(data: any | null): Observable<User> {
    return this.http.put<User>(this.url + '/user/update', data);
  }

  delUser(id: string) {
    return this.http.delete<string>(`${this.url}/user/delete/${id}`);
  }

  addDataRole(data: any | null): Observable<User> {
    return this.http.post<User>(this.url + '/user/addToRole', data);
  }
}