import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Questions } from './questions';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class QuestionsService {
  constructor(private http: HttpClient) {}

  private url = 'http://localhost:8081/api';
  getData() {
    return this.http.get<Questions[]>(`${this.url}/questions`);
  }
  getQuestion(id: string) {
    console.log(`${this.url}/question/${id}`);
    return this.http.get<Questions>(`${this.url}/question/${id}`);
  }
  addData(data: any | null): Observable<Questions> {
    return this.http.post<Questions>(`${this.url}/question/addToSubject`, data);
  }
  updateQuestion(question: Questions) {
    return this.http.put<Questions>(`${this.url}/question/update`, question);
  }
  delQuestion(id: string) {
    return this.http.delete(`${this.url}/questions/delete/${id}`);
  }
}
