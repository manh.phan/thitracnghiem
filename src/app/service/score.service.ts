import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Score } from './score';

@Injectable({
  providedIn: 'root',
})
export class ScoreService {
  private url = 'http://localhost:8081/api';

  constructor(private http: HttpClient) {}

  getData() {
    return this.http.get<Score[]>(`${this.url}/scores`);
  }
  getSubject(id: string) {
    return this.http.get<Score>(`${this.url}/score/${id}`);
  }
  addData(score: Score) {
    return this.http.post<Score>(`${this.url}/score/save`, score);
  }
  updateSubject(score: Score) {
    return this.http.put<Score>(`${this.url}/score/update`, score);
  }
  delSubject(id: string) {
    return this.http.delete(`${this.url}/score/delete/${id}`);
  }
}
