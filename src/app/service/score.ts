import { Subject } from "./questions";
import { User } from "./user";

export interface Score {
    id: string,
    user: User,
    subject: Subject,
    score: number
}