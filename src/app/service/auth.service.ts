import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor() {}

  isAuthenticated() {
    const promise = new Promise<boolean>((resolve, rejects) => {
      setTimeout(() => {
        resolve(localStorage.getItem('token')?.length ? true : false);
      }, 800);
    });
    return promise;
  }
  Login(token: string) {
    localStorage.setItem('token', token);
  }

  logout() {
    localStorage.clear();
  }
}
