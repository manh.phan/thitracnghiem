export interface User {
  id: string;
  username: string;
  password: string;
  name: string;
  email: string;
  role: {id:string, name:string};
}

export const User = ['id', 'username', 'password', 'name', 'email', 'role', 'edit'];
