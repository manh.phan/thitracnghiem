import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class SubjectService {
  private url = 'http://localhost:8081/api';

  constructor(private http: HttpClient) {}

  getData() {
    return this.http.get<Subject[]>(`${this.url}/subjects`);
  }
  getSubject(id: string) {
    return this.http.get<Subject>(`${this.url}/subject/${id}`)
  }
  addData(subject: any) {
    return this.http.post<Subject>(`${this.url}/subject/save`, subject);
  }
  updateSubject(subject: Subject) {
    return this.http.put<Subject>(`${this.url}/subject/update`, subject);
  }
  delSubject(id: string) {
    return this.http.delete(`${this.url}/subject/delete/${id}`);
  }
}

export interface Subject {
  id: string;
  name: string;
}
