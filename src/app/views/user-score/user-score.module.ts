import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserScoreComponent } from './user-score.component';



@NgModule({
  declarations: [
    UserScoreComponent
  ],
  imports: [
    CommonModule
  ]
})
export class UserScoreModule { }
