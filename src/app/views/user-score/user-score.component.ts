import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ScoreService } from 'src/app/service/score.service';

@Component({
  selector: 'app-user-score',
  templateUrl: './user-score.component.html',
  styleUrls: ['./user-score.component.scss'],
})
export class UserScoreComponent implements OnInit {
  constructor(
    private scoreService: ScoreService,
    private activatedRoute: ActivatedRoute
  ) {}
  id!: string;
  score!: number;
  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id') || '';
    console.log(this.id);
    this.scoreService.getData().subscribe(data => {
      for (const item of data) {
        console.log(item.id)
        if(item.id == this.id) {
          this.score = item.score;
          break
        }
      }
    })
  }
}
