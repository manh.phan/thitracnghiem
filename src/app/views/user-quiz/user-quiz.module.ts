import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserQuizComponent } from './user-quiz.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatRadioModule } from '@angular/material/radio';


@NgModule({
  declarations: [UserQuizComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRadioModule,
  ],
})
export class UserQuizModule {}
