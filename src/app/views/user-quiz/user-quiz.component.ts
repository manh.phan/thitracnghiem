import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Questions } from 'src/app/service/questions';

import { QuestionsService } from 'src/app/service/questions.service';
import { Score } from 'src/app/service/score';
import { ScoreService } from 'src/app/service/score.service';
import { UsersService } from 'src/app/service/users.service';

@Component({
  selector: 'app-user-quiz',
  templateUrl: './user-quiz.component.html',
  styleUrls: ['./user-quiz.component.scss'],
})
export class UserQuizComponent implements OnInit {
  users: any;
  quizs: any;
  questions = new Array<Questions>();
  questionsSubmit: any;
  numberScore: number = 0;
  favoriteSeason = new Array(5);
  private date = new Date(new Date().getTime() + 1000 * 60 * 15);
  minutes = 0;
  seconds = 0;

  id!: string;

  score!: Score;
  constructor(
    private user: UsersService,
    private question: QuestionsService,
    private scoreService: ScoreService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id') || '';
    this.question.getData().subscribe((data) => {
      data.forEach((element) => {
        if (element.subject.id == this.id) this.questions.push(element);
      });
      if (this.questions.length >= 5)
        this.questions = this.getRandom(this.questions, 5);
      console.log(this.questions);
    });
    for (let i = 0; i < this.favoriteSeason.length; i++)
      this.favoriteSeason[i] = 0;
    this.getTime();
  }

  addDiem(form: any) {
    this.numberScore = 0;
    this.questionsSubmit = form.value;
    for (const key in this.questionsSubmit) {
      if (Object.prototype.hasOwnProperty.call(this.questionsSubmit, key)) {
        const element = this.questionsSubmit[key];
        for (const question of this.questions) {
          if (question.id == key) {
            if (question.correctIndex == element) this.numberScore++;
          }
        }
      }
    }
    var id = localStorage.getItem('token') || '';
    this.user.getuser(id).subscribe((data) => {
      this.score = {
        id: '',
        user: data,
        subject: this.questions[0].subject,
        score: (this.numberScore * 100) / this.questions.length,
      };
      this.scoreService.addData(this.score).subscribe((data) => {
        this.router.navigateByUrl(`/score/${data.id}`);
      });
    });
  }

  getRandom(arr: any, n: any) {
    var result = new Array(n),
      len = arr.length,
      taken = new Array(len);
    if (n > len)
      throw new RangeError('getRandom: more elements taken than available');
    while (n--) {
      var x = Math.floor(Math.random() * len);
      result[n] = arr[x in taken ? taken[x] : x];
      taken[x] = --len in taken ? taken[len] : len;
    }
    return result;
  }

  getTime() {
    setInterval(() => {
      let date = new Date();
      if (this.date > date) {
        let dateLine = new Date(this.date.getTime() - date.getTime());
        this.seconds = dateLine.getSeconds();
        this.minutes = dateLine.getMinutes();
        // console.log(this.favoriteSeason);
      } else {
        this.router.navigateByUrl('/module');
      }
    }, 1000);
  }
}
