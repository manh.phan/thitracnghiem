import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { QuestionsService } from 'src/app/service/questions.service';
import { SubjectService } from 'src/app/service/subject.service';
import { UsersService } from '../../../service/users.service';

@Component({
  selector: 'app-question-add',
  templateUrl: './question-add.component.html',
  styleUrls: ['./question-add.component.scss'],
})
export class QuestionAddComponent implements OnInit {
  hide = true;
  questionForm = this.formBuilder.group({
    question: ['', [Validators.required]],
    answers1: ['', [Validators.required]],
    answers2: ['', [Validators.required]],
    answers3: ['', [Validators.required]],
    answers4: ['', [Validators.required]],
    correctIndex: ['', [Validators.required]],
    subjectID: ['', [Validators.required]],
  });

  data = {
    question: '',
    answers1: '',
    answers2: '',
    answers3: '',
    answers4: '',
    correctIndex: '',
    subject: {},
  };
  subject: Subject[] = [];
  constructor(
    private formBuilder: FormBuilder,
    private subjectService: SubjectService,
    private questionsService: QuestionsService
  ) {}

  ngOnInit(): void {
    this.subjectService.getData().subscribe((data) => {
      this.subject = data;
    });
  }

  addUser() {
    this.data.question = this.questionForm.get('question')?.value;
    this.data.answers1 = this.questionForm.get('answers1')?.value;
    this.data.answers2 = this.questionForm.get('answers2')?.value;
    this.data.answers3 = this.questionForm.get('answers3')?.value;
    this.data.answers4 = this.questionForm.get('answers4')?.value;
    this.data.correctIndex = this.questionForm.get('correctIndex')?.value;
    for (const sb of this.subject) {
      if (sb.id.toString() == this.questionForm.get('subjectID')?.value) {
        this.data.subject = sb;
      }
    }
    this.questionsService.addData(this.data).subscribe((data) => {
    });
    this.questionForm.reset();
  }

  clearUser() {
    this.questionForm.reset();
  }
}

interface Subject {
  id: string;
  name: string;
}