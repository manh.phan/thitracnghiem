import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../../../service/questions.service';
import { Questions } from '../../../service/questions';
import { DataSource } from '@angular/cdk/collections';
import { Observable, ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.scss'],
})
export class QuestionListComponent implements OnInit {
  displayedColumns = Questions.concat('edit');
  dataToDisplay: Questions[] = [];
  dataSource = new ExampleDataSource(this.dataToDisplay);

  constructor(private questionsService: QuestionsService) {
    this.questionsService.getData().subscribe((data) => {
      this.dataToDisplay = data;
      for (const i in data) {
        this.dataToDisplay[i].subject = data[i].subject || {
          id: -1,
          name: 'null',
        };
      }
      this.dataSource.setData(this.dataToDisplay);
    });
  }

  ngOnInit(): void {
  }

  delQuestion(id: string) {
    this.questionsService.delQuestion(id).subscribe(
      (result) => {
        this.dataToDisplay = this.dataToDisplay.filter((item) => item.id != id);
        this.dataSource.setData(this.dataToDisplay);
      },
      (error) => {
        console.log(123);
      }
    );
  }
}

class ExampleDataSource extends DataSource<Questions> {
  private _dataStream = new ReplaySubject<Questions[]>();

  constructor(initialData: Questions[]) {
    super();
    this.setData(initialData);
  }

  connect(): Observable<Questions[]> {
    return this._dataStream;
  }

  disconnect() {}

  setData(data: Questions[]) {
    this._dataStream.next(data);
  }
}