import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuestionAddComponent } from './question-add/question-add.component';
import { QuestionEditComponent } from './question-edit/question-edit.component';
import { QuestionListComponent } from './question-list/question-list.component';

const routes: Routes = [
  {
    path: 'list',
    component: QuestionListComponent,
  },
  {
    path: 'add',
    component: QuestionAddComponent,
  },
  {
    path: 'edit/:id',
    component: QuestionEditComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuestionRoutingModule {}
