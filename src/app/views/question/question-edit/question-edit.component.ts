import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Questions, Subject } from 'src/app/service/questions';
import { QuestionsService } from 'src/app/service/questions.service';
import { SubjectService } from 'src/app/service/subject.service';

@Component({
  selector: 'app-question-edit',
  templateUrl: './question-edit.component.html',
  styleUrls: ['./question-edit.component.scss'],
})
export class QuestionEditComponent implements OnInit {
  id!: string;
  questionForm = this.formBuilder.group({
    question: ['', [Validators.required]],
    answers1: ['', [Validators.required]],
    answers2: ['', [Validators.required]],
    answers3: ['', [Validators.required]],
    answers4: ['', [Validators.required]],
    correctIndex: ['', [Validators.required]],
    subjectID: ['', [Validators.required]],
  });
  data!: Questions;
  subject: Subject[] = [];
  constructor(
    private formBuilder: FormBuilder,
    private subjectService: SubjectService,
    private activatedRoute: ActivatedRoute,
    private questionsServe: QuestionsService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.subjectService.getData().subscribe((data) => {
      this.subject = data;
    });
    this.id = this.activatedRoute.snapshot.paramMap.get('id') || '';
    this.questionsServe.getQuestion(this.id).subscribe((data) => {
      this.questionForm.get('question')?.setValue(data.question);
      this.questionForm.get('answers1')?.setValue(data.answers1);
      this.questionForm.get('answers2')?.setValue(data.answers2);
      this.questionForm.get('answers3')?.setValue(data.answers3);
      this.questionForm.get('answers4')?.setValue(data.answers4);
      this.questionForm.get('correctIndex')?.setValue(data.correctIndex);
      this.questionForm.get('subjectID')?.setValue(data.subject.id);
      this.questionForm.get('subjectID')?.disable();
      this.data = data;
    });
  }

  editQuestion() {
    this.data.question = this.questionForm.get('question')?.value;
    this.data.answers1 = this.questionForm.get('answers1')?.value;
    this.data.answers2 = this.questionForm.get('answers2')?.value;
    this.data.answers3 = this.questionForm.get('answers3')?.value;
    this.data.answers4 = this.questionForm.get('answers4')?.value;
    this.data.correctIndex = this.questionForm.get('correctIndex')?.value;
    console.log(this.data);
    this.questionsServe
      .updateQuestion(this.data)
      .subscribe(() => this.location.back());
  }

  clearData() {
    this.location.back();
  }
}
