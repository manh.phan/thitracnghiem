import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable, ReplaySubject } from 'rxjs';
import { ScoreService } from 'src/app/service/score.service';
import { Score } from 'src/app/service/score';
import { UsersService } from 'src/app/service/users.service';

@Component({
  selector: 'app-user-scores',
  templateUrl: './user-scores.component.html',
  styleUrls: ['./user-scores.component.scss'],
})
export class UserScoresComponent implements OnInit {
  constructor(
    private scoreService: ScoreService,
    private usersService: UsersService
  ) {}
  score: Score[] = [];
  displayedColumns = ['id', 'username', 'name', 'subject', 'score'];
  dataToDisplay: Score[] = [];
  dataSource = new ExampleDataSource(this.dataToDisplay);
  ngOnInit(): void {
    this.scoreService.getData().subscribe((data) => {
      console.log(localStorage.getItem('token'));
      let idUser = localStorage.getItem('token');
      for (const item of data) {
        if (item.user.id == idUser) {
          this.score.push(item);
        }
      }
      this.dataToDisplay = this.score;
      this.dataSource.setData(this.dataToDisplay);
    });
  }

  delUser(id: string) {
    this.usersService.delUser(id).subscribe(
      (result) => {
        this.dataToDisplay = this.dataToDisplay.filter((item) => item.id != id);
        this.dataSource.setData(this.dataToDisplay);
      },
      (error) => {
        console.log(123);
      }
    );
  }
}

class ExampleDataSource extends DataSource<Score> {
  private _dataStream = new ReplaySubject<Score[]>();

  constructor(initialData: Score[]) {
    super();
    this.setData(initialData);
  }

  connect(): Observable<Score[]> {
    return this._dataStream;
  }

  disconnect() {}

  setData(data: Score[]) {
    this._dataStream.next(data);
  }
}
