import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UsersService } from '../../../service/users.service';
import { User } from 'src/app/service/user';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss'],
})
export class UserEditComponent implements OnInit {
  hide = true;
  userForm = this.formBuilder.group({
    username: ['', ],
    name: ['', [Validators.required]],
    'new-password': ['', [Validators.required, Validators.minLength(6)]],
    'current-password': ['', [Validators.required, Validators.minLength(6)]],
    email: ['', [Validators.required, Validators.email]],
    role: ['', [Validators.required]],
  });

  data!: User;

  dataRole = {
    userID: '',
    roleID: '',
  };

  id: string = '';

  constructor(
    private formBuilder: FormBuilder,
    private usersService: UsersService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id') || '';
    this.usersService.getuser(this.id).subscribe((data) => {
      this.data = data;
      this.userForm.get('username')?.setValue(data.username);
      this.userForm.get('username')?.disable();
      this.userForm.get('name')?.setValue(data.name);
      this.userForm.get('email')?.setValue(data.email);
      this.userForm.get('new-password')?.setValue(data.password);
      this.userForm.get('current-password')?.setValue(data.password);
      this.userForm.get('role')?.setValue(data.role.name);
      this.userForm.get('role')?.disable();
    });
  }

  editUser() {
    this.data.name = this.userForm.get('name')?.value;
    this.data.password = this.userForm.get('new-password')?.value;
    this.data.email = this.userForm.get('email')?.value;

    this.usersService.updateUser(this.data).subscribe((data) => {
      console.log(data)
    });
  }

  clearUser() {
    this.location.back();
  }
}
