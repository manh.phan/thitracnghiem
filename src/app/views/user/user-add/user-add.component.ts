import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { UsersService } from '../../../service/users.service';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss'],
})
export class UserAddComponent implements OnInit {
  hide = true;
  userForm = this.formBuilder.group({
    username: ['', [Validators.required]],
    name: ['', [Validators.required]],
    'new-password': ['', [Validators.required, Validators.minLength(6)]],
    'current-password': ['', [Validators.required, Validators.minLength(6)]],
    email: ['', [Validators.required, Validators.email]],
    role: ['', [Validators.required]],
  });

  data = {
    username: '',
    name: '',
    password: '',
    email: '',
    role: null,
  };

  dataRole = {
    userID: '',
    roleID: '',
  };
  constructor(
    private formBuilder: FormBuilder,
    private usersService: UsersService
  ) {}

  ngOnInit(): void {}

  addUser() {
    this.data.username = this.userForm.get('username')?.value;
    this.data.name = this.userForm.get('name')?.value;
    this.data.password = this.userForm.get('new-password')?.value;
    this.data.email = this.userForm.get('email')?.value;
    this.userForm.get('role')?.value == 'admin'
      ? (this.dataRole.roleID = '1844901d-ce44-4b96-895a-e661a6f842bc')
      : (this.dataRole.roleID = 'efa6aecd-3ac7-45e6-88bb-29a30801d7a3');
    this.usersService.addData(this.data).subscribe((data) => {
      if (data != null) {
        this.dataRole.userID = data.id.toString();
        this.usersService.addDataRole(this.dataRole).subscribe((dt) => {
          console.log(dt);
        });
      } else {
        console.log('Lỗi thêm người dùng');
      }
    });
    this.userForm.reset();
  }

  clearUser() {
    this.userForm.reset();
  }
}

// {
//   "username": "username",
//   "name": "Phan Manh",
//   "password": "sonaka123",
//   "email": "admin@gmail.com",
//   "role": "admin"
// }
