import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../service/users.service';
import { DataSource } from '@angular/cdk/collections';
import { Observable, ReplaySubject } from 'rxjs';
import { User } from '../../../service/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {
  displayedColumns = User;
  dataToDisplay: User[] = [];
  dataSource = new ExampleDataSource(this.dataToDisplay);

  constructor(private usersService: UsersService) {
    this.usersService.getData().subscribe((data) => {
      this.dataToDisplay = data;
      this.dataSource.setData(this.dataToDisplay);
    });
  }

  ngOnInit(): void {}

  delUser(id: string) {
    this.usersService.delUser(id).subscribe(
      (result) => {
        this.dataToDisplay = this.dataToDisplay.filter((item) => item.id != id);
        this.dataSource.setData(this.dataToDisplay);
      },
      (error) => {
        console.log(123);
      }
    );
  }
}

class ExampleDataSource extends DataSource<User> {
  private _dataStream = new ReplaySubject<User[]>();

  constructor(initialData: User[]) {
    super();
    this.setData(initialData);
  }

  connect(): Observable<User[]> {
    return this._dataStream;
  }

  disconnect() {}

  setData(data: User[]) {
    this._dataStream.next(data);
  }
}