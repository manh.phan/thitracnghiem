import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { UserInfoComponent } from './user-info.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [UserInfoComponent],
  imports: [CommonModule, MatIconModule, BrowserModule, MatButtonModule],
})
export class UserInfoModule {}
