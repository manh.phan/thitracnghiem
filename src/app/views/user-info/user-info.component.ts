import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/service/user';
import { UsersService } from 'src/app/service/users.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss'],
})
export class UserInfoComponent implements OnInit {
  userInfo: User = {
    id: '0',
    username: 'username',
    password: 'password',
    name: 'name',
    email:'email@gmail.com',
    role: {
      id: '0',
      name: 'role'
    }

  };
  constructor(private usersService: UsersService) {}

  ngOnInit(): void {
    console.log(localStorage.getItem('token'));
    this.usersService
      .getuser(localStorage.getItem('token') || '')
      .subscribe((data) => {
        this.userInfo = data;
        console.log(data);
      });
  }
}
