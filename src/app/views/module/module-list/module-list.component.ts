import { Component, OnInit } from '@angular/core';
import { Subject, SubjectService } from '../../../service/subject.service';
import { DataSource } from '@angular/cdk/collections';
import { Observable, ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-module-list',
  templateUrl: './module-list.component.html',
  styleUrls: ['./module-list.component.scss'],
})
export class ModuleListComponent implements OnInit {
  displayedColumns = ['id', 'name', 'edit'];
  dataToDisplay: Subject[] = [];
  dataSource = new ExampleDataSource(this.dataToDisplay);

  rowClick(row: any) {
    console.log(row);
  }

  constructor(private subjectService: SubjectService) {
    this.subjectService.getData().subscribe((data) => {
      this.dataToDisplay = data;
      this.dataSource.setData(this.dataToDisplay);
    });
  }

  ngOnInit(): void {}

  delSubject(id: string) {
    this.subjectService.delSubject(id).subscribe(
      result => {
        this.dataToDisplay = this.dataToDisplay.filter((item) => item.id != id);
        this.dataSource.setData(this.dataToDisplay);
      },
      (error) => {
        console.log(123)
      }
    );
  }
}

class ExampleDataSource extends DataSource<Subject> {
  private _dataStream = new ReplaySubject<Subject[]>();

  constructor(initialData: Subject[]) {
    super();
    this.setData(initialData);
  }

  connect(): Observable<Subject[]> {
    return this._dataStream;
  }

  disconnect() {}

  setData(data: Subject[]) {
    this._dataStream.next(data);
  }
}
