import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { SubjectService, Subject } from 'src/app/service/subject.service';

@Component({
  selector: 'app-module-edit',
  templateUrl: './module-edit.component.html',
  styleUrls: ['./module-edit.component.scss'],
})
export class ModuleEditComponent implements OnInit {
  hide = true;
  subjectForm = this.formBuilder.group({
    name: ['', [Validators.required]],
  });

  id!: string;
  data!: Subject;

  constructor(
    private formBuilder: FormBuilder,
    private subjectService: SubjectService,
    private activatedRoute: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id') || '';
    this.subjectService.getSubject(this.id).subscribe((data) => {
      this.data = data;
      this.subjectForm.get('name')?.setValue(data.name);
    });
  }

  updateSubject() {
    this.data.name = this.subjectForm.get('name')?.value;
    this.subjectService
      .updateSubject(this.data)
      .subscribe(() => this.location.back());
  }

  clearData() {
    this.location.back();
  }
}
