import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { SubjectService, Subject } from 'src/app/service/subject.service';

@Component({
  selector: 'app-module-add',
  templateUrl: './module-add.component.html',
  styleUrls: ['./module-add.component.scss'],
})
export class ModuleAddComponent implements OnInit {
  hide = true;
  subjectForm = this.formBuilder.group({
    name: ['', [Validators.required]],
  });

  data!: Subject;

  constructor(
    private formBuilder: FormBuilder,
    private subjectService: SubjectService
  ) {}

  ngOnInit(): void {}

  addUser() {
    this.subjectService.addData({name: this.subjectForm.get('name')?.value}).subscribe((data) => {
    });
    this.subjectForm.reset();
  }

  clearUser() {
    this.subjectForm.reset();
  }
}
