import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModuleListComponent } from './module-list/module-list.component';
import { ModuleAddComponent } from './module-add/module-add.component';
import { ModuleEditComponent } from './module-edit/module-edit.component';

const routes: Routes = [
  {
    path: 'list',
    component: ModuleListComponent,
  },
  {
    path: 'add',
    component: ModuleAddComponent,
  },
  {
    path: 'edit/:id',
    component: ModuleEditComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModuleRoutingModule {}
