import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../service/users.service';
import { DataSource } from '@angular/cdk/collections';
import { Observable, ReplaySubject } from 'rxjs';
import { User } from '../../../service/user';
import { ScoreService } from 'src/app/service/score.service';
import { Score } from 'src/app/service/score';

@Component({
  selector: 'app-score-list',
  templateUrl: './score-list.component.html',
  styleUrls: ['./score-list.component.scss'],
})
export class ScoreListComponent implements OnInit {
  displayedColumns = ['id', 'username', 'name', 'subject', 'score'];
  dataToDisplay: Score[] = [];
  dataSource = new ExampleDataSource(this.dataToDisplay);

  constructor(
    private usersService: UsersService,
    private scoreService: ScoreService
  ) {
    this.scoreService.getData().subscribe((data) => {
      this.dataToDisplay = data;
      this.dataSource.setData(this.dataToDisplay);
    });
  }

  ngOnInit(): void {}

  delUser(id: string) {
    this.usersService.delUser(id).subscribe(
      (result) => {
        this.dataToDisplay = this.dataToDisplay.filter((item) => item.id != id);
        this.dataSource.setData(this.dataToDisplay);
      },
      (error) => {
        console.log(123);
      }
    );
  }
}

class ExampleDataSource extends DataSource<Score> {
  private _dataStream = new ReplaySubject<Score[]>();

  constructor(initialData: Score[]) {
    super();
    this.setData(initialData);
  }

  connect(): Observable<Score[]> {
    return this._dataStream;
  }

  disconnect() {}

  setData(data: Score[]) {
    this._dataStream.next(data);
  }
}
