import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { UserSubjectRoutingModule } from './user-subject-routing.module';
import { UserSubjectListComponent } from './user-subject-list/user-subject-list.component';

@NgModule({
  declarations: [UserSubjectListComponent],
  imports: [
    CommonModule,
    UserSubjectRoutingModule,
    MatCardModule,
    MatDividerModule,
    MatProgressBarModule,
    MatButtonModule,
    MatGridListModule,
    DragDropModule,
  ],
})
export class UserSubjectModule {}
