import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserSubjectListComponent } from './user-subject-list/user-subject-list.component';

const routes: Routes = [{
  path: "",
  component: UserSubjectListComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserSubjectRoutingModule { }
