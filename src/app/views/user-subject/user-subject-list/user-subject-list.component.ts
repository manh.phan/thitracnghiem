import { Component, OnInit } from '@angular/core';
import { Subject } from 'src/app/service/questions';
import { SubjectService } from 'src/app/service/subject.service';

@Component({
  selector: 'app-user-subject-list',
  templateUrl: './user-subject-list.component.html',
  styleUrls: ['./user-subject-list.component.scss'],
})
export class UserSubjectListComponent implements OnInit {
  constructor(private subjectSevie: SubjectService) {}
  data!: Subject[];

  ngOnInit(): void {
    this.subjectSevie.getData().subscribe((data) => {
      this.data = data;
    });
  }
}
