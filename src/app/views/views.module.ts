import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserModule } from './user/user.module';
import { ModuleModule } from './module/module.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { QuestionModule } from './question/question.module';
import { UserSubjectModule } from './user-subject/user-subject.module';
import { UserQuizModule } from './user-quiz/user-quiz.module';
import { ScoreModule } from './score/score.module';
import { UserInfoModule } from './user-info/user-info.module';
import { UserScoreModule } from './user-score/user-score.module';
import { UserScoresModule } from './user-scores/user-scores.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DashboardModule,
    UserModule,
    ModuleModule,
    QuestionModule,
    UserSubjectModule,
    UserQuizModule,
    ScoreModule,
    UserInfoModule,
    UserScoreModule,
    UserScoresModule
  ],
})
export class ViewsModule {}
