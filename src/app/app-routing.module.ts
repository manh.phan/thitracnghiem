import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './routes/auth-guard';
import { AdminLayoutComponent } from './shared/components/layouts/admin-layout/admin-layout.component';
import { LoginComponent } from './shared/components/layouts/login/login.component';
import { LogoutComponent } from './shared/components/layouts/logout/logout.component';
import { NotFoundComponent } from './shared/components/layouts/not-found/not-found.component';
import { UserLayoutComponent } from './shared/components/layouts/user-layout/user-layout.component';
import { UserInfoComponent } from './views/user-info/user-info.component';
import { UserQuizComponent } from './views/user-quiz/user-quiz.component';
import { UserScoreComponent } from './views/user-score/user-score.component';
import { UserScoresComponent } from './views/user-scores/user-scores.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  {
    path: '',
    component: UserLayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'module' },
      {
        path: 'module',
        loadChildren: () =>
          import('./views/user-subject/user-subject-routing.module').then(
            (m) => m.UserSubjectRoutingModule
          ),
      },
      {
        path: 'quiz',
        pathMatch: 'full',
        redirectTo: 'module',
      },
      {
        path: 'quiz/:id',
        canActivate: [AuthGuard],
        component: UserQuizComponent,
      },
      { path: 'info', canActivate: [AuthGuard], component: UserInfoComponent },
      {
        path: 'score/:id',
        canActivate: [AuthGuard],
        component: UserScoreComponent,
      },
      {
        path: 'score',
        canActivate: [AuthGuard],
        component: UserScoresComponent,
      },
    ],
  },
  {
    path: 'admin',
    component: AdminLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./views/dashboard/dashboard-routing.module').then(
            (m) => m.DashboardRoutingModule
          ),
      },
      { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
      {
        path: 'module',
        loadChildren: () =>
          import('./views/module/module-routing.module').then(
            (m) => m.ModuleRoutingModule
          ),
      },
      {
        path: 'question',
        loadChildren: () =>
          import('./views/question/question-routing.module').then(
            (m) => m.QuestionRoutingModule
          ),
      },
      {
        path: 'user',
        loadChildren: () =>
          import('./views/user/user-routing.module').then(
            (m) => m.UserRoutingModule
          ),
      },
      {
        path: 'score',
        loadChildren: () =>
          import('./views/score/score-routing.module').then(
            (m) => m.ScoreRoutingModule
          ),
      },
    ],
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
